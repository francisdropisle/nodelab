# The image is built on top of one that has node preinstalled
FROM node:latest
WORKDIR /usr/src/app
COPY . .
RUN npm install
EXPOSE 3000
CMD [ "node", "main.js" ]